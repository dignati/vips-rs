use std::path::Path;
use std::ptr;
use std::ffi::{CStr, CString};
use std::os::raw::{c_void};

use {init, raw, Error};

pub struct Image {
    raw: *mut raw::VipsImage,
}

impl Drop for Image {
    fn drop(&mut self) {
        unsafe {
            raw::g_object_unref(self.raw as *mut c_void);
        }
    }
}

impl Image {
    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<Image, Error> {
        init();
        use std::os::unix::prelude::*;
        let path = CString::new(path.as_ref().as_os_str().as_bytes())?;
        unsafe {
            let ret = raw::vips_image_new_from_file(path.as_ptr(), 0);
            if ret.is_null() {
                Err(Error::from_str("Loading image failed"))
            } else {
                Ok(Image { raw: ret })
            }
        }
    }

    pub fn get_width(&self) -> i32 {
        unsafe {
            raw::vips_image_get_width(self.raw)
        }
    }

    pub fn get_height(&self) -> i32 {
        unsafe {
            raw::vips_image_get_height(self.raw)
        }
    }

    pub fn get_bands(&self) -> i32 {
        unsafe {
            raw::vips_image_get_bands(self.raw)
        }
    }

    pub fn get_x_resolution(&self) -> f64 {
        unsafe {
            raw::vips_image_get_xres(self.raw)
        }
    }

    pub fn get_y_resolution(&self) -> f64 {
        unsafe {
            raw::vips_image_get_yres(self.raw)
        }
    }

    pub fn get_x_offset(&self) -> i32 {
        unsafe {
            raw::vips_image_get_xoffset(self.raw)
        }
    }

    pub fn get_y_offset(&self) -> i32 {
        unsafe {
            raw::vips_image_get_yoffset(self.raw)
        }
    }

    pub fn get_filename(&self) -> String {
        unsafe {
            let filename = raw::vips_image_get_filename(self.raw);
            CStr::from_ptr(filename).to_string_lossy().into_owned()
        }
    }

    pub fn get_scale(&self) -> f64 {
        unsafe {
            raw::vips_image_get_scale(self.raw)
        }
    }

    pub fn get_offset(&self) -> f64 {
        unsafe {
            raw::vips_image_get_offset(self.raw)
        }
    }

    pub fn get_metadata_string(&self, name: &str) -> Result<String, Error> {
        unsafe {
            let name = CString::new(name)?;
            let mut value_ptr = ptr::null_mut();
            let ret = raw::vips_image_get_as_string(self.raw, name.as_ptr(), &mut value_ptr);
            if ret == 0 {
                let metadata = CStr::from_ptr(value_ptr).to_string_lossy().into_owned();
                raw::g_free(value_ptr as *mut c_void);
                Ok(metadata)
            } else {
                Err(Error::from_str("Metadata not found"))
            }
        }
    }

    pub fn remove_metadata(&mut self, name: &str) -> Result<bool, Error> {
        unsafe {
            let c_name = CString::new(name)?;
            Ok(raw::vips_image_remove(self.raw, c_name.as_ptr()) == 1)
        }
    }

    pub fn get_metadata_fields(&self) -> Vec<String> {
        unsafe {
            let arr = raw::vips_image_get_fields(self.raw);
            let mut ptr = arr;
            let mut res = Vec::new();
            while !(*ptr).is_null() {
                res.push(CStr::from_ptr(*ptr).to_string_lossy().into_owned());
                ptr = ptr.offset(1);
            }
            raw::g_strfreev(arr);
            res
        }
    }

    pub fn append_history(&mut self, operation: &str) -> Result<(), Error> {
        unsafe {
            let operation = CString::new(operation)?;
            let ret = raw::vips_image_history_printf(self.raw, operation.as_ptr());
            if ret == 0 {
                Ok(())
            } else {
                Err(Error::from_str("Appending to image history failed"))
            }
        }
    }

    pub fn get_history(&self) -> String {
        unsafe {
            let ptr = raw::vips_image_get_history(self.raw);
            CStr::from_ptr(ptr).to_string_lossy().into_owned()
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::env;
    #[test]
    fn load_image() {
        let mut img = Image::from_file("test/test.png").unwrap();
        println!("{:?}", img.get_metadata_fields());
        println!("{:?}", img.get_metadata_string("filename"));
        img.append_history("Test operation").unwrap();
        println!("{:?}", img.get_history());
    }
}