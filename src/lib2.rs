extern crate libvips_sys;

use libvips_sys as ffi;
use std::ffi::{CStr, CString};
use std::os::raw::c_char;
use std::sync::{Once, ONCE_INIT};


fn init() {
    static INIT: Once = ONCE_INIT;
    INIT.call_once(|| {
        ffi::vips_init(c_name);
    });
}

#[derive(Debug)]
pub enum VipsError {
    NullOnInit,
    InvalidString,
}

impl From<std::ffi::NulError> for VipsError {
    fn from(_: std::ffi::NulError) -> VipsError {
        VipsError::InvalidString
    }
}

pub struct Vips {
    app_name: String,
    errors: *const c_char,
    version: String,
}

impl Vips {
    pub fn new<T>(app_name: T) -> Result<Vips, VipsError>
    where
        T: Into<String>,
    {
        let name = app_name.into();
        let c_name = CString::new(name.clone().as_str())?.as_ptr();
        unsafe {
            Ok(Vips {
                app_name: name,
                errors: ffi::vips_error_buffer(),
                version: CStr::from_ptr(ffi::vips_version_string())
                    .to_string_lossy()
                    .into_owned(),
            })
        }
    }

    pub fn get_errors(&self) -> String {
        unsafe { CStr::from_ptr(self.errors).to_string_lossy().into_owned() }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_app_name() {
        let vips = Vips::new("test_app_name").unwrap();
        assert_eq!(vips.app_name, "test_app_name");
    }

    #[test]
    fn test_err_buffer() {
        let vips = Vips::new("test").unwrap();
        assert!(vips.get_errors().is_empty());
    }
}
