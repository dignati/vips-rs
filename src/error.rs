use std::ffi::{NulError};
#[derive(Debug,PartialEq)]
pub struct Error {
    message: String,
}

impl Error {
    pub fn from_str(s: &str) -> Error {
        Error {
            message: s.to_string(),
        } 
    }
}

impl From<NulError> for Error {
    fn from(_: NulError) -> Error {
        Error::from_str("data contained a nul byte that could not be \
                         represented as a string")
    }
}