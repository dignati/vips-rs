extern crate libvips_sys as raw;

use std::env;
use std::ffi::{CString};
use std::sync::{Once, ONCE_INIT};

mod image;
mod error;

pub use image::{Image};
pub use error::Error;

fn init() {
    static INIT: Once = ONCE_INIT;
    INIT.call_once(|| {
        let app_name = env::args().nth(0).unwrap();
        let c_name = CString::new(app_name.as_str()).unwrap().as_ptr(); 
        unsafe {
            raw::vips_init(c_name);
        }
    });
}

#[cfg(test)]
mod test {
    #[test]
    fn call_init_works() {
        super::init();
    }
}