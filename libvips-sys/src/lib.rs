#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

#[cfg(test)]
mod tests {
    use super::*;
    use std::ffi::{CStr, CString};

    #[test]
    fn sanity_check() {
        let app_name = CString::new("test").unwrap();
        let img_name = CString::new("test.png").unwrap();
        unsafe {
            vips_init(app_name.as_ptr());
            let err_buf = vips_error_buffer();

            let img = vips_image_new_from_file(img_name.as_ptr(), 0);
            let width = vips_image_get_width(img);
            assert_eq!(width, 300);
            let error_msgs = CStr::from_ptr(err_buf).to_string_lossy().into_owned();
            assert!(error_msgs.is_empty());
            vips_shutdown();
        }
    }
}