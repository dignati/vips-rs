extern crate bindgen;
extern crate pkg_config;

use std::env;
use std::path::PathBuf;

fn get_compiler_config() -> Vec<String> {
    pkg_config::Config::new()
        .probe("vips")
        .unwrap()
        .include_paths
        .iter()
        .map(|p| String::from("-I") + &p.clone().into_os_string().into_string().unwrap())
        .collect()
}

fn main() {
    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());
    let out_path = out_dir.join("bindings.rs");
    let compiler_config = get_compiler_config();
    bindgen::Builder::default()
        .clang_args(compiler_config)
        .generate_comments(true)
        .header("wrapper.h")
        .blacklist_type("FP_NAN")
        .blacklist_type("FP_INFINITE")
        .blacklist_type("FP_ZERO")
        .blacklist_type("FP_SUBNORMAL")
        .blacklist_type("FP_NORMAL")
        .generate()
        .expect("Unable to generate bindings")
        .write_to_file(out_path)
        .expect("Couldn't write bindings!");
}